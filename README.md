# Description

Container image for OpenLDAP 2.4.56 on Alpine 3.13.
It includes ppolicy overlay and is configured for password hashing for clear text passwords.

SSL is required. Set the container hostname to the domain name of the issued certificate.

# Build

```
podman build . -t quay.io/tbalsys/openldap:2.4.56-r2`
podman push quay.io/tbalsys/openldap:2.4.56-r2
```

# Run

```
podman run --rm \
  --name ldap \
  --hostname ldap.my-domain.com \
  -e LDAP_BASE_DN=dc=my-domain,dc=com \
  -e LDAP_ROOT_DN=cn=Manager,dc=my-domain,dc=com \
  -e LDAP_ROOT_PW=admin \
  -e LDAP_SSL_CA=/certs/ldap.my-domain.com/fullchain.cer \
  -e LDAP_SSL_CERT=/certs/ldap.my-domain.com/ldap.my-domain.com.cer \
  -e LDAP_SSL_KEY=/certs/ldap.my-domain.com/ldap.my-domain.com.key \
  -v ./certs:/certs \
  -v ./openldap-data:/var/lib/openldap/openldap-data \
  -p 636:636 \
  quay.io/tbalsys/openldap
```

# Environment

| Variable      | Description                                                                 |
| ------------- | --------------------------------------------------------------------------- |
| LDAP_BASE_DN  | base dn (dc=my-domain,dc=com)                                               |
| LDAP_ROOT_DN  | Admin username (cn=Manager,dc=my-domain,dc=com)                             |
| LDAP_ROOT_PW  | Admin password                                                              |
| LDAP_SSL_CA   | Path to certificate authority file (fullchain.cer for Let's Encrypt)        |
| LDAP_SSL_CERT | Path to certificate file                                                    |
| LDAP_SSL_KEY  | Path to certificate key file                                                |

# Volumes

The path `/var/lib/openldap/openldap-data` should be mounted as a volume for database persistence.

# Ports

Service listens on port `636`. 

# Commands available inside container

Modify resources

```
ldapadd \
  -H ldaps://$HOSTNAME \
  -D "$LDAP_ROOT_DN" \
  -w "$LDAP_ROOT_PW" \
  < file.ldif
```

Retrieve resources

```
ldapsearch \
  -b $LDAP_BASE_DN \
  -H ldaps://$HOSTNAME \
  -D $LDAP_ROOT_DN \
  -w "$LDAP_ROOT_PW" \
  '(objectclass=*)'
```

| Option        | Description                                                                 |
| ------------- | --------------------------------------------------------------------------- |
| -b searchbase | Use searchbase as the starting point for the search instead of the default  |
| -H ldapuri    | URI(s) referring to the ldap server(s)                                      |
| -D binddn     | Use the Distinguished Name binddn to bind to the LDAP directory             |
| -w passwd     | Use passwd as the password for simple authentication                        |
