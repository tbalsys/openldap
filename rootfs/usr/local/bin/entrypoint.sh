#!/bin/sh

: ${LDAP_BASE_DN:?}
: ${LDAP_ROOT_DN:?}
: ${LDAP_ROOT_PW:?}
: ${LDAP_SSL_CA:?}
: ${LDAP_SSL_CERT:?}
: ${LDAP_SSL_KEY:?}

sed \
  -e "s#cn=Manager,dc=my-domain,dc=com#$LDAP_ROOT_DN#" \
  -e "s#dc=my-domain,dc=com#$LDAP_BASE_DN#" \
  -e "s#secret#\"$(slappasswd -ns "$LDAP_ROOT_PW")\"#" \
  -e "s#/etc/ssl/certs/ca-certificates.crt#$LDAP_SSL_CA#" \
  -e "s#/etc/ssl/certs/ldap.cer#$LDAP_SSL_CERT#" \
  -e "s#/etc/ssl/private/ldap.key#$LDAP_SSL_KEY#" \
  -i /etc/openldap/slapd.conf

chown ldap:ldap /var/lib/openldap/openldap-data

exec $@
