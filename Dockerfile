FROM alpine:3.13

EXPOSE 636

VOLUME /var/lib/openldap/openldap-data

RUN apk add --no-cache openldap openldap-back-mdb openldap-overlay-ppolicy openldap-clients && \
  mkdir /var/lib/openldap/run && \
  chown ldap:ldap /var/lib/openldap/run && \
  sed -i 's/# moduleload\s\+back_mdb\.la/moduleload back_mdb.so/' /etc/openldap/slapd.conf

ADD rootfs /

ENTRYPOINT ["entrypoint.sh"]

CMD ["slapd", "-h", "ldaps:///", "-u", "ldap", "-g", "ldap", "-d", "256"]
